﻿using System;
using System.Data;
using System.Net.Http;
using MySql.Data.MySqlClient;

namespace NT2_API.Models
{
    public static class Utiles
    {
        static MySqlConnection _Conexion;
        public enum TipoDato { String, Integer, Boolean, Date }

        public const int MinutosVidaMaximaToken = 600;

        public static MySqlConnection ObtenerConexion()
        {
            if (_Conexion == null)
            {
                _Conexion = new MySqlConnection();
                _Conexion.ConnectionString = "Server=190.210.9.72;Uid=logiciel;Pwd=lsi1789;Database=logiciel;";
            }
            if (_Conexion.State != ConnectionState.Open)
            {
                try
                {
                    _Conexion.Open();
                }
                catch (SystemException error)
                {
                }
            }
            return _Conexion;
        }

        public static HttpResponseMessage AgregarCabeceras(HttpResponseMessage RespuestaAProcesar)
        {
            RespuestaAProcesar.Headers.Add("Access-Control-Allow-Origin", "*");
            return RespuestaAProcesar;
        }

        public static DataTable ObtenerDTDeComando(MySqlCommand comando)
        {
            MySqlDataAdapter Adaptador = new MySqlDataAdapter(comando);
            DataTable DTADevolver = new DataTable();
            Adaptador.Fill(DTADevolver);
            return DTADevolver;
        }

    }
}
