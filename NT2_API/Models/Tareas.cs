﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace NT2_API.Models
{
    public static class Tareas
    {

        public static bool CrearOGuardar(Tarea Tarea, ref String Error)
        {
            bool Devolver;
            using (MySqlCommand Comando = new MySqlCommand())
            {
                Comando.Connection = Utiles.ObtenerConexion();
                Comando.CommandType = CommandType.Text;
                Comando.CommandText = "update tareas set titulo=@titulo, detalle=@detalle where id=@id";
                Comando.Parameters.AddWithValue("@id", Tarea.Id);
                Comando.Parameters.AddWithValue("@titulo", Tarea.Titulo);
                Comando.Parameters.AddWithValue("@detalle", Tarea.Detalle);

                try
                {
                    int CantidadRegistros = Comando.ExecuteNonQuery();
                    if (CantidadRegistros == 0)
                    {
                        //La tarea no existía, intento hacer el insert
                        Comando.CommandText = "insert into tareas (titulo, detalle) values (@titulo, @detalle)";
                    }
                    try
                    {
                        Comando.ExecuteNonQuery();
                        Devolver = true;
                    }
                    catch (SystemException error)
                    {
                        Devolver = false;
                        Error = "Error al insertar: " + error.Message;
                    }
                }
                catch (SystemException error)
                {
                    Error = "Error al actualizar: " + error.Message;
                    Devolver = false;
                }
                Comando.Connection.Close();
            }
            return Devolver;
        }


        public static bool Eliminar(int Id, ref string Error)
        {
            bool Devolver;
            using (MySqlCommand Comando = new MySqlCommand())
            {
                Comando.Connection = Utiles.ObtenerConexion();
                Comando.CommandType = CommandType.Text;
                Comando.CommandText = "delete from tareas where id=@id";
                Comando.Parameters.AddWithValue("@id", Id);

                try
                {
                    int CantidadRegistros=Comando.ExecuteNonQuery();
                    if (CantidadRegistros>0)
                    {
                        Devolver = true;
                    } else
                    {
                        Devolver = false;
                        Error = "Id inexistente";
                    }
                }
                catch (SystemException error)
                {
                    Error = "Error al eliminar: " + error.Message;
                    Devolver = false;
                }
                Comando.Connection.Close();
            }
            return Devolver;
        }


        public static List<Tarea> obtenerTodas()
        {
            List<Tarea> Devolver = new List<Tarea>();

            using (MySqlCommand Comando = new MySqlCommand())
            {
                Comando.Connection = Utiles.ObtenerConexion();
                Comando.CommandType = CommandType.Text;
                Comando.CommandText = "select * from tareas";

                DataTable dtTareas = Utiles.ObtenerDTDeComando(Comando);
                foreach(DataRow UnaFila in dtTareas.Rows)
                {
                    Devolver.Add(DataRowATarea(UnaFila));
                }
                Comando.Connection.Close();
            }
            return Devolver;
        }

        static Tarea DataRowATarea(DataRow FilaAProcesar)
        {
            Tarea Devolver = new Tarea()
            {
                Id = Convert.ToInt32(FilaAProcesar["Id"]),
                Titulo = Convert.ToString(FilaAProcesar["Titulo"]),
                Detalle = Convert.ToString(FilaAProcesar["Detalle"])
            };

            return Devolver;
        }

    }



    public class Tarea
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Detalle { get; set; }
    }
}
