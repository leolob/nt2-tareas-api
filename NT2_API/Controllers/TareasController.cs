﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Http;
using MySql.Data.MySqlClient;
using NT2_API.Models;
using System.Net.Http;
using System.Net;

namespace NT2_API.Controllers
{
    [RoutePrefix("api/tareas")]
    public class TareasController : ApiController
    {

        [HttpGet]
        [Route("getTodas")]
        public IHttpActionResult getTodas()
        {
            HttpResponseMessage Respuesta;
            string Error = "";

            List<Tarea> TareasObtenidas = Tareas.obtenerTodas();

            Respuesta = Request.CreateResponse(HttpStatusCode.OK, TareasObtenidas);

            return ResponseMessage(Utiles.AgregarCabeceras(Respuesta));
        }

        [HttpDelete]
        [Route("eliminar")]
        public IHttpActionResult eliminar(int Id=-1)
        {
            HttpResponseMessage Respuesta;
            string Error = "";

            if (Id>0)
            {
                if (Tareas.Eliminar(Id, ref Error))
                {
                    Respuesta = Request.CreateResponse(HttpStatusCode.OK);
                } else
                {
                    Respuesta = Request.CreateResponse(HttpStatusCode.BadRequest, new { Detalle= "Error al eliminar - "+Error });
                }
            } else
            {
                Respuesta = Request.CreateResponse(HttpStatusCode.BadRequest, new { Detalle = "No se recibió Id de tarea" });
            }

            return ResponseMessage(Utiles.AgregarCabeceras(Respuesta));

        }

        [HttpPost]
        [Route("crearoactualizar")]
        public IHttpActionResult crearoactualizar(string titulo=null, string detalle=null, int Id = -1)
        {
            HttpResponseMessage Respuesta;
            string Error = "";

            if (titulo != null && detalle != null)
            {
                if (titulo!="" && detalle!=null)
                {
                    if (Tareas.CrearOGuardar(new Tarea { Detalle = detalle, Id = Id, Titulo = titulo }, ref Error))
                    {
                        Respuesta = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        Respuesta = Request.CreateResponse(HttpStatusCode.BadRequest, new { Detalle = "Error al CrearOActualizar - " + Error });
                    }
                }
                else
                {
                    Respuesta = Request.CreateResponse(HttpStatusCode.BadRequest, new { Detalle = "No se recibió título o detalle" });
                }
            }
            else
            {
                Respuesta = Request.CreateResponse(HttpStatusCode.BadRequest, new { Detalle = "No se recibió título o detalle" });
            }

            return ResponseMessage(Utiles.AgregarCabeceras(Respuesta));
        }

    }
}
